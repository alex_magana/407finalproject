#include <string.h>
#include "logging.h"

FILE * Logger::logger = fopen("Logging.txt", "w");

DataFlowException::DataFlowException(const char *type, const char *error)
{
    sprintf(msg, "Throwing exception: (%s), %s", type, error);
    Logger::LogEvent(msg);
}

void Logger::LogEvent(const char *event)
{
    fwrite(event, sizeof(char), strlen(event), logger);
}

void Logger::Finalize()
{
    fclose(logger);
}