#include <stdio.h>
#include "source.h"
#include "sink.h"

#ifndef FILTERS_H
#define FILTERS_H

class Filter: public source, public sink
{
public:
    virtual void Update();
    virtual const char *FilterName() = 0;
    virtual const char *SourceName() {return FilterName(); };
    virtual const char *SinkName() {return FilterName(); };
};

/*-----------------------------------------------------*/

//class filter : public source, public sink
//{
//public:
//    void Update();
//};

class Shrinker : public Filter
{
public:
    virtual const char *FilterName() {return "Shrinker";};
    void Execute();
};

class LRConcat : public Filter
{
public:
    virtual const char *FilterName() {return "LRConcat";};
    void Execute();
};

class TBConcat : public Filter
{
public:
    virtual const char *FilterName() {return "TBConcat"; };
	void Execute();
};

class Blender : public Filter
{
protected:
    float factor;
public:
    virtual const char *FilterName() {return "Blender"; };
	void SetFactor(float factor);
	void Execute();
};

class Crop : public Filter {
public:
    Crop()  { Istart = Istop = Jstart = Jstop = -1; };
    virtual const char *FilterName() { return "Crop"; };
    virtual void   Execute();
    void           SetRegion(int Istart_, int Istop_, int Jstart_, int Jstop_)
    {
        Istart = Istart_;
        Istop  = Istop_;
        Jstart = Jstart_;
        Jstop  = Jstop_;
    }
    
private:
    int Istart, Istop, Jstart, Jstop;
};

class Transpose : public Filter {
public:
    virtual const char *FilterName() {return "Transpose"; };
    void Execute();
};

class Invert : public Filter {
public:
    virtual const char *FilterName() {return "Invert"; };
    void Execute();
};

class Color: public source {
public:
    const char *SourceName() {return "Constant Color"; };
    Color(int w, int h, unsigned char r, unsigned char g, unsigned char b)
    {
        width = w;
        height = h;
        red = r;
        green = g;
        blue = b;
    };
    void Execute();
    void Update();
    
protected:
    int width;
    int height;
    unsigned char red;
    unsigned char green;
    unsigned char blue;
    
};

class CheckSum: public sink {
public:
    const char *SinkName() {return "CheckSum"; };
    void OutputCheckSum(const char *file);
};

#endif