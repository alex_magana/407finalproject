#include "image.h"

#ifndef SOURCE_H
#define SOURCE_H

class source
{
protected:
    Image image;
    virtual void Execute() = 0;
    
public:
    Image * GetOutput();
    source();
    virtual void Update() = 0;
    virtual const char *SourceName() = 0;
};

#endif