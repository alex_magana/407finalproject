#include "image.h"

#ifndef SINK_H
#define SINK_H

class sink
{
protected:
    const Image *img;
    const Image *img2;
public:
    void SetInput(const Image *img);
    void SetInput2(const Image *img2);
    virtual const char *SinkName() = 0;
    sink();
};

#endif