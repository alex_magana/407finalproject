#include <stdio.h>
#include <stdlib.h>
#include "source.h"
#include "image.h"

Image::Image()
{
    int width = 0;
    int height = 0;
    int maxval =0;
    unsigned char * buffer = NULL;
}

void Image::setSource(source *s)
{
    src = s;
}

void Image::Update() const
{
    if(src != NULL)
    {
        src->Update();
    }
}

void Image::setHeight(int h)
{
    height = h;
}

void Image::setWidth(int w)
{
    width = w;
}

void Image::setBuffer()
{
    buffer = (unsigned char*) malloc(sizeof(unsigned char)*3*height*width);
}

int Image::getWidth() const
{
    return width;
}

int Image::getHeight() const
{
    return height;
}

unsigned char* Image::getBuffer() const
{
    return buffer;
}
