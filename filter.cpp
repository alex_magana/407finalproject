#include <stdio.h>
#include <iostream>
#include "source.h"
#include "sink.h"
#include "filters.h"
#include "logging.h"

using namespace std;

void Filter::Update()
{
    if(img != NULL)
    {
        char msg[128];
        sprintf(msg, "%s: about to update input1\n", SourceName());
//        fprintf(stderr, "About to update the input\n");
        Logger::LogEvent(msg);
        img->Update();
        sprintf(msg, "%s: done updating input1\n", SourceName());
//        fprintf(stderr, "Done updating the input\n");
        Logger::LogEvent(msg);
    }
    if(img2 != NULL)
    {
        char msg[128];
        sprintf(msg, "%s: about to update input2\n", SourceName());
//        fprintf(stderr, "About to update input2\n");
        Logger::LogEvent(msg);
        img2->Update();
        sprintf(msg, "%s: done updating input2\n", SourceName());
//        fprintf(stderr, "Done updating input2\n");
        Logger::LogEvent(msg);
    }
    
    char msg[128];
    sprintf(msg, "%s: about to excute: \n", FilterName());
//    cerr << "About to execute: " << FilterName() << endl;
    Execute();
    sprintf(msg, "%s: done executing \n", FilterName());
//    cerr << "Done executing: " << FilterName() << endl;
}