#include <stdio.h>
#include "source.h"
#include "sink.h"
#include "filters.h"
#include "logging.h"
#include <iostream>

using namespace std;

void Shrinker::Execute()
{
	int newWidth = img->getWidth()/2;
	int newHeight = img->getHeight()/2;
    image.setHeight(newHeight);
	image.setWidth(newWidth);
	image.setBuffer();
    
	for(int i = 0; i < image.getHeight(); i++)
	{
		for(int j = 0; j < image.getWidth(); j++)
		{
			int temp = 3*(i*image.getWidth()+j);
			int temp2 = 3*(i*2*img->getWidth()+j*2);
			image.getBuffer()[temp] = img->getBuffer()[temp2];
			image.getBuffer()[temp+1] = img->getBuffer()[temp2+1];
			image.getBuffer()[temp+2] = img->getBuffer()[temp2+2];
		}
	}
}

void LRConcat::Execute()
{
	int height = 0;
    
    if (img == NULL || img2 == NULL)
    {
        char msg[1024];
        sprintf(msg, "%s: one of the two inputs is NULL\n", SinkName());
        DataFlowException e(SinkName(), msg);
        throw e;
    }
    
	int newWidth = img->getWidth() + img2->getWidth();
    
	if(img->getHeight() != img2->getHeight())
	{
        char msg[1024];
        sprintf(msg, "%s: heights must match: %d, %d\n", SinkName(), img->getHeight(), img2->getHeight());
        DataFlowException e(SinkName(), msg);
        throw e;
	}
    else {
		height = img->getHeight();
	}
	
	image.setHeight(height);
    image.setWidth(newWidth);
	image.setBuffer();
    
	for(int i = 0; i < image.getHeight(); i++)
	{
		for(int j = 0; j < image.getWidth(); j++)
		{
			int temp = 3*(i*image.getWidth()+j);
			if(j < img->getWidth())
			{
				int temp2 = 3*(i*img->getWidth()+j);
				image.getBuffer()[temp] = img->getBuffer()[temp2];
				image.getBuffer()[temp+1] = img->getBuffer()[temp2+1];
				image.getBuffer()[temp+2] = img->getBuffer()[temp2+2];
			} else {
				int temp2 = 3*((i*img2->getWidth()+j) - img->getWidth());
				image.getBuffer()[temp] = img2->getBuffer()[temp2];
				image.getBuffer()[temp+1] = img2->getBuffer()[temp2+1];
				image.getBuffer()[temp+2] = img2->getBuffer()[temp2+2];
			}
		}
	}
}

void TBConcat::Execute()
{
	int width = 0;
	int newHeight = img->getHeight() + img2->getHeight();
	
    if (img == NULL || img2 == NULL)
    {
        char msg[1024];
        sprintf(msg, "%s: one of the two inputs is NULL\n", SinkName());
        DataFlowException e(SinkName(), msg);
        throw e;
    }
    
	if(img->getWidth() != img2->getWidth())
	{
        char msg[1024];
        sprintf(msg, "%s: widths must match: %d, %d\n", SinkName(), img->getWidth(), img2->getWidth());
        DataFlowException e(SinkName(), msg);
        throw e;
	}
    else {
		width = img->getWidth();
	}
	
    image.setHeight(newHeight);
	image.setWidth(width);
	image.setBuffer();
    
	for(int j = 0; j < image.getWidth(); j++)
	{
		for(int i = 0; i < img->getHeight(); i++)
		{
            int temp = 3*(i*image.getWidth()+j);
            int temp2 = 3*(i*image.getWidth()+j);
            image.getBuffer()[temp] = img->getBuffer()[temp2];
            image.getBuffer()[temp+1] = img->getBuffer()[temp2+1];
            image.getBuffer()[temp+2] = img->getBuffer()[temp2+2];
        }
    }
	for(int j = 0; j < image.getWidth(); j++)
    {
        for(int i = 0; i < img2->getHeight(); i++)
        {
            int temp = 3*((i+img->getHeight())*image.getWidth()+j);
            int temp2 = 3*(i*image.getWidth()+j);
            image.getBuffer()[temp] = img2->getBuffer()[temp2];
            image.getBuffer()[temp+1] = img2->getBuffer()[temp2+1];
            image.getBuffer()[temp+2] = img2->getBuffer()[temp2+2];
        }
	}
}

void Blender::SetFactor(float factor)
{
    this->factor = factor;
}

void Blender::Execute()
{
	image.setHeight(img->getHeight());
    image.setWidth(img->getWidth());
	image.setBuffer();
    
    if (factor < 0 || factor > 1){
        char msg[1024];
        sprintf(msg, "%s: Invalid factor for Blender: %f\n", SinkName(), factor);
        DataFlowException e(SinkName(), msg);
        throw e;
    }
    else
        
	for(int i = 0; i < image.getHeight(); i++)
	{
		for(int j = 0; j < image.getWidth(); j++)
		{
            int temp = 3*(i*image.getWidth()+j);
            int temp2 = 3*(i*img->getWidth()+j);
            image.getBuffer()[temp] = img->getBuffer()[temp2]*(factor) + img2->getBuffer()[temp2]*(1-factor);
            image.getBuffer()[temp+1] = img->getBuffer()[temp2+1]*(factor) + img2->getBuffer()[temp2+1]*(1-factor);
            image.getBuffer()[temp+2] = img->getBuffer()[temp2+2]*(factor) + img2->getBuffer()[temp2+2]*(1-factor);
		}
	}
}

void Crop::Execute()
{
//    OneInputCheck();
    if (Istart < 0 || Istop < 0 || Jstart < 0 || Jstop < 0)
    {
        char msg[1024];
        sprintf(msg, "%s: uninitialized region", FilterName());
        DataFlowException e(FilterName(), msg);
        throw e;
    }
    if (Istart >= img->getWidth() || Istop >= img->getWidth())
    {
        char msg[1024];
        sprintf(msg, "%s: region outside image width", FilterName());
        DataFlowException e(FilterName(), msg);
        throw e;
    }
    if (Jstart >= img->getHeight() || Jstop >= img->getHeight())
    {
        char msg[1024];
        sprintf(msg, "%s: region outside image height", FilterName());
        DataFlowException e(FilterName(), msg);
        throw e;
    }
    if (Istop < Istart || Jstop < Jstart)
    {
        char msg[1024];
        sprintf(msg, "%s: invalid region", FilterName());
        DataFlowException e(FilterName(), msg);
        throw e;
    }
    
    int height = Jstop-Jstart+1;
    int width  = Istop-Istart+1;
    int inputWidth = img->getWidth();
//    image.SetSize(width, height);
    image.setHeight(height);
    image.setWidth(width);
    image.setBuffer();
//    unsigned char *buffer = image.getBuffer();
//    const unsigned char *buffer1 = img->getBuffer();
    for (int i = Istart ; i <= Istop ; i++)
    {
        for (int j = Jstart ; j <= Jstop ; j++)
        {
            int temp = j*inputWidth+i;
            int temp2  = (j-Jstart)*width+(i-Istart);
            image.getBuffer() [3*temp2]   = img->getBuffer() [3*temp];
            image.getBuffer() [3*temp2+1] = img->getBuffer() [3*temp+1];
            image.getBuffer() [3*temp2+2] = img->getBuffer() [3*temp+2];
        }
    }
}

void Transpose::Execute()
{
    if (img == NULL) {
        char msg[1024];
        sprintf(msg, "%s: no input\n", FilterName());
        DataFlowException e(SinkName(), msg);
        throw e;
    }
    
    image.setHeight(img->getWidth());
    image.setWidth(img->getHeight());
    image.setBuffer();
    
    for (int i = 0; i < image.getHeight(); i++) {
        for (int j = 0; j < image.getWidth(); j++) {
            int temp = 3*(i*image.getWidth()+j);
			int temp2 = 3*(j*img->getWidth()+i);
			image.getBuffer()[temp] = img->getBuffer()[temp2];
			image.getBuffer()[temp+1] = img->getBuffer()[temp2+1];
			image.getBuffer()[temp+2] = img->getBuffer()[temp2+2];
        }
    }
}

void Invert::Execute()
{
    if (img == NULL) {
        char msg[1024];
        sprintf(msg, "%s: no input\n", FilterName());
        DataFlowException e(SinkName(), msg);
        throw e;
    }
    
    image.setHeight(img->getHeight());
    image.setWidth(img->getWidth());
    image.setBuffer();
    
    for(int i = 0; i < image.getWidth(); i++)
	{
		for(int j = 0; j < image.getHeight(); j++)
		{
            int temp = 3*(j*image.getWidth()+i);
            int temp2 = 3*(j*img->getWidth()+i);
            image.getBuffer()[temp]   = 255 - img->getBuffer()[temp2];
            image.getBuffer()[temp+1] = 255 - img->getBuffer()[temp2+1];
            image.getBuffer()[temp+2] = 255 - img->getBuffer()[temp2+2];
		}
	}
}

void Color::Update()
{
    Execute();
}

void Color::Execute()
{
    image.setHeight(height);
    image.setWidth(width);
    image.setBuffer();
    
    for (int i = 0; i < image.getWidth(); i++)
    {
        for (int j = 0; j < image.getHeight(); j++) {
            int temp = 3*(j*image.getWidth() + i);
            image.getBuffer() [temp] = red;
            image.getBuffer() [temp+1] = green;
            image.getBuffer() [temp+2] = blue;
        }
    }
}

void CheckSum::OutputCheckSum(const char *file)
{
    FILE *f = fopen(file, "w");
    unsigned char red = 0;
    unsigned char green = 0;
    unsigned char blue = 0;
    
    for (int i = 0; i < img->getWidth(); i++)
    {
        for (int j = 0; j < img->getHeight(); j++)
        {
            int temp = 3*(j*img->getWidth()+i);
            red += img->getBuffer() [temp];
            green += img->getBuffer() [temp+1];
            blue += img->getBuffer() [temp+2];
        }
    }
    
    fprintf(f, "CHECKSUM: %d , %d, %d", red, green, blue);
    fclose(f);
}


