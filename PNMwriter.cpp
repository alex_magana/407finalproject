#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "PNMwriter.h"
#include "logging.h"
#include <iostream>

using namespace std;

void PNMwriter::Write(char *filename)
{
	FILE *out_file = fopen(filename, "wb");
    if (out_file == NULL) {
        fprintf(stderr, "Unable to open file %s\n", filename);
    }
    
	fprintf(out_file, "P6\n%d %d\n255\n", img->getWidth(), img->getHeight());
	fwrite(img->getBuffer(), sizeof(unsigned char), 3*img->getHeight()*img->getWidth(), out_file);
    
	fclose(out_file);
}

//const char *PNMwriter::SinkName()
//{
//    const char *msg;
//    msg = "PNMwriter";
//    return msg;
//}