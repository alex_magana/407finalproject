#include <stdio.h>

#ifndef IMAGE_H
#define IMAGE_H

class source;
class Image
{
private:
    unsigned char *buffer;
    int width, height, maxval;
    source *src;

public:
    void setHeight(int h);
    void setWidth(int w);
    void setBuffer();
    
    int getHeight() const;
    int getWidth() const;
    unsigned char* getBuffer() const;
    void Update() const;
    void setSource(source *s);
	
    /* Constructors */
    Image();
    Image(int h, int w, unsigned char *b);
    Image(Image &img);
};

#endif
