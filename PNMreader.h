#include <stdio.h>
#include "source.h"

#ifndef PNMREADER_H
#define PNMREADER_H

class PNMreader : public source
{
protected:
    char *file;
public:
    PNMreader(char *file);
    virtual void Execute();
    virtual void Update();
    virtual const char *SourceName() {return "Reader"; };
};

#endif
